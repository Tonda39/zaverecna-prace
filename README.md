
# Zaverecna Prace

Repozitář do předmětu SSP, obsahující projekt do předmětu SKJ. Repozitář obsahuje kontrolu markdown souborů pomocí mdl a kontrolu syntaxe a linting pro python soubory pomocí flake8. Stránky na které se umísťují výsledky z flake8 jsou [zde](https://tonda39.gitlab.io/zaverecna-prace/index.html). Dále také obsahuje dva issue templaty.

## Spuštění projektu

1) Instalace závislostí

```bash
pip install -r requirements.txt
```

2) Spuštění serveru

```bash
cd Knihovna/
python manage.py runserver
```

3) Přechod na webové rozhraní

- v prohlížeči otevřít `http://127.0.0.1:8000`
