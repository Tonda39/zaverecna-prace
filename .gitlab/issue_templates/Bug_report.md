# Bug Report

## Summary

<!-- Provide a brief summary of the bug. -->

## Steps to Reproduce

<!-- Describe the steps to reproduce the issue. -->

1. Step one
2. Step two
3. ...

## Expected Behavior

<!-- Describe what you expected to happen. -->

## Actual Behavior

<!-- Describe what actually happened. -->

## Screenshots

<!-- Add screenshots to help explain your problem, if applicable. -->

## Additional Context

<!-- Add any other context about the problem here. -->
