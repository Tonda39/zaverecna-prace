"""
URL configuration for Knihovna project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from base import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name = 'index'),
    path('books/', views.books, name = 'books'),
    path('authors/', views.authors, name = 'authors'),
    path('publishers/', views.publishers, name = 'publishers'),
    path('customers/', views.customers, name = 'customers'),
    path('borrows/', views.borrows, name = 'borrows'),
    path('genres/', views.genres, name = 'genres'),
    path('author/<int:author_id>/', views.author, name = 'author'),
    path('book/<int:book_id>/', views.book, name = 'book'),
    path('publisher/<int:publisher_id>/', views.publisher, name = 'publisher'),
    path('genre/<int:genre_id>/', views.genre, name = 'genre'),
    path('customer/<int:customer_id>/', views.customer, name = 'customer'),
    path('borrow/<int:borrow_id>/', views.borrow, name = 'borrow'), 
    path('publisherform/', views.publisherform, name = 'publisherform'),
    path('addpublisher/', views.addpublisher, name = 'addpublisher'),
    path('genreform/', views.genreform, name = 'genreform'),
    path('addgenre/', views.addgenre, name = 'addgenre'),
    path('authorform/', views.authorform, name = 'authorform'),
    path('addauthor/', views.addauthor, name = 'addauthor'),
    path('customerform/', views.customerform, name = 'customerform'),
    path('addcustomer/', views.addcustomer, name = 'addcustomer'),
    path('bookform/', views.bookform, name = 'bookform'),
    path('addbook/', views.addbook, name = 'addbook'),
    path('borrowform/', views.borrowform, name = 'borrowform'),
    path('addborrow/', views.addborrow, name = 'addborrow'),
    path('removebook/<int:entity_id>/', views.removebook, name = 'removebook'),
    path('removeauthor/<int:entity_id>/', views.removeauthor, name = 'removeauthor'),
    path('removeborrow/<int:entity_id>/', views.removeborrow, name = 'removeborrow'),
    path('removecustomer/<int:entity_id>/', views.removecustomer, name = 'removecustomer'),
    path('removegenre/<int:entity_id>/', views.removegenre, name = 'removegenre'),
    path('removepublisher/<int:entity_id>/', views.removepublisher, name = 'removepublisher')

    
    # path('account/', views.account, name = 'account')

    # path('company/<int:company_id>/', views.company, name = 'company'),
    # path('addcompany/', views.addcompany, name = 'addcompany')
]
