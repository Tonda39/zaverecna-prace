from django.contrib import admin
from .models import Book, Author, Genre, Publisher, Customer, Borrow

# Register your models here.

admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Publisher)
admin.site.register(Customer)
admin.site.register(Borrow)