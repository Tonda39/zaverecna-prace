from django import forms
from .models import Book, Author, Publisher, Customer, Borrow, Genre


class PublisherForm(forms.ModelForm):
    class Meta:
        model = Publisher
        fields = ['name'] 

class GenreForm(forms.ModelForm):
    
    class Meta:
        model = Genre
        fields = ['name']

class AuthorForm(forms.ModelForm):
    
    class Meta:
        model = Author
        fields = ['name', 'surname', 'nationality', 'date_of_birth', 'date_of_death']

class CustomerForm(forms.ModelForm):
    
    class Meta:
        model = Customer
        fields = ['name', 'surname', 'email', 'phone']

class BookForm(forms.ModelForm):
    
    class Meta:
        model = Book
        fields = ['name', 'author', 'genre', 'date_of_release', 'publisher']

class BorrowForm(forms.ModelForm):

    class Meta:
        model = Borrow
        fields = ['book', 'customer', 'start_date', 'end_date']