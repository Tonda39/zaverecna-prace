from django.db import models

# Create your models here.

class Author(models.Model):
    name = models.CharField(max_length = 30)
    surname = models.CharField(max_length = 30)
    nationality = models.CharField(max_length = 20)
    date_of_birth = models.DateField()
    date_of_death = models.DateField(null = True, blank = True)

    def __str__(self):
        return f"{self.name} {self.surname}"
    

class Book(models.Model):
    name = models.CharField(max_length = 100)
    date_of_release = models.DateField()
    # total = models.IntegerField()
    # available = models.IntegerField()
    author = models.ForeignKey('Author', null = True, blank = True, on_delete = models.CASCADE)    
    genre = models.ForeignKey('Genre', on_delete = models.CASCADE)
    publisher = models.ForeignKey('Publisher', null = True, blank = True, on_delete = models.CASCADE)
    # publisher = models.ManyToManyField('Publisher',  blank = True)

    def __str__(self):
        return f"{self.name}  ({self.author}) " 


class Genre(models.Model):
    name = models.CharField(max_length = 20)

    def __str__(self):
        return self.name

# class Employee(models.Model):
#     name = models.CharField(max_length = 30)
#     surname = models.CharField(max_length = 30)

class Publisher(models.Model):
    name = models.CharField(max_length = 50)
    
    def __str__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length = 30)
    surname = models.CharField(max_length = 30)
    email = models.CharField(max_length = 50)
    phone = models.IntegerField()

    def __str__(self):
        return f"{self.name} {self.surname}"

class Borrow(models.Model):
    book = models.ForeignKey('Book', on_delete = models.CASCADE)
    customer = models.ForeignKey('Customer', on_delete = models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return f"{self.book} - {self.customer}, {self.start_date} - {self.end_date}"