from django.shortcuts import render, get_object_or_404, redirect
from .models import Book, Author, Publisher, Customer, Borrow, Genre
from .forms import *


# Create your views here.

def index(request):
    # companies = Company.objects.all()
    # company_form = CompanyForm()

    return render(request, 'base/index.html', {})
# 'companies': companies, 'company_form': company_form

def books(request):
    books = Book.objects.all()
    
    return render(request, 'base/books.html', {'books': books})

def book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    
    return render(request, 'base/book.html', {'book': book})
    
def bookform(request):
    form = BookForm()
    
    return render(request, 'base/bookform.html', {'form': form})

def addbook(request):
    if request.method == "POST":
        form = BookForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            author = form.cleaned_data['author']
            date_of_release = form.cleaned_data['date_of_release']
            publisher = form.cleaned_data['publisher']
            genre = form.cleaned_data['genre']
            entity = Book(name = name, author = author, date_of_release = date_of_release, publisher = publisher, genre = genre)
            entity.save()

            return redirect('books')

def removebook(request, entity_id):
    entity = get_object_or_404(Book, pk=entity_id)

    entity.delete()

    return redirect('books')
    
    
def authors(request):
    authors = Author.objects.all()
    
    return render(request, 'base/authors.html', {'authors': authors})

def author(request, author_id):
    author = get_object_or_404(Author, pk=author_id)
    books = Book.objects.filter(author = author)
    
    return render(request, 'base/author.html', {'books': books, 'author': author})

def authorform(request):
    author_form = AuthorForm()
    
    return render(request, 'base/authorform.html', {'author_form': author_form})

def addauthor(request):
    if request.method == "POST":
        author_form = AuthorForm(request.POST)
        if author_form.is_valid():
            name = author_form.cleaned_data['name']
            surname = author_form.cleaned_data['surname']
            date_of_birth = author_form.cleaned_data['date_of_birth']
            date_of_death = author_form.cleaned_data['date_of_death']
            nationality = author_form.cleaned_data['nationality']
            author = Author(name = name, surname = surname, nationality = nationality, date_of_birth = date_of_birth, date_of_death = date_of_death)
            author.save()

            return redirect('authors')
    
def removeauthor(request, entity_id):
    entity = get_object_or_404(Author, pk=entity_id)

    entity.delete()

    return redirect('authors')

def publishers(request):
    publishers = Publisher.objects.all()
    
    return render(request, 'base/publishers.html', {'publishers': publishers})

def publisher(request, publisher_id):
    publisher = get_object_or_404(Publisher, pk=publisher_id)
    books = Book.objects.filter(publisher = publisher)
    
    return render(request, 'base/publisher.html', {'publisher': publisher, 'books': books})

def publisherform(request):
    publisher_form = PublisherForm()

    return render(request, 'base/publisherform.html', {"publisher_form": publisher_form})

def addpublisher(request):
    if request.method == "POST":
        publisher_form = PublisherForm(request.POST)
        if publisher_form.is_valid():
            name = publisher_form.cleaned_data['name']
            publisher = Publisher(name = name)
            publisher.save()

            return redirect('publishers')

def removepublisher(request, entity_id):
    entity = get_object_or_404(Publisher, pk=entity_id)

    entity.delete()

    return redirect('publishers')

def customers(request):
    customers = Customer.objects.all()
    
    return render(request, 'base/customers.html', {'customers': customers})

def customer(request, customer_id):
    customer = get_object_or_404(Customer, pk=customer_id)
    borrows = Borrow.objects.filter(customer = customer)

    return render(request, 'base/customer.html', {'customer': customer, 'borrows': borrows})

def customerform(request):
    customer_form = CustomerForm()
    
    return render(request, 'base/customerform.html', {'customer_form': customer_form})

def addcustomer(request):
    if request.method == "POST":
        form = CustomerForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            surname = form.cleaned_data['surname']
            email = form.cleaned_data['email']
            phone = form.cleaned_data['phone']
            entity = Customer(name = name, surname = surname, email = email, phone = phone)
            entity.save()

            return redirect('customers')
    
def removecustomer(request, entity_id):
    entity = get_object_or_404(Customer, pk=entity_id)

    entity.delete()

    return redirect('customers')

def borrows(request):
    borrows = Borrow.objects.all()
    
    return render(request, 'base/borrows.html', {'borrows': borrows})

def borrow(request, borrow_id):
    borrow = get_object_or_404(Borrow, pk=borrow_id)
    customer = get_object_or_404(Customer, pk = borrow.customer.id)
    book = get_object_or_404(Book, pk = borrow.book.id)
    
    return render(request, 'base/borrow.html', {'borrow': borrow, 'customer': customer, 'book': book})

def borrowform(request):
    form = BorrowForm()
    
    return render(request, 'base/borrowform.html', {'form': form})

def addborrow(request):
    if request.method == "POST":
        form = BorrowForm(request.POST)
        if form.is_valid():
            book = form.cleaned_data['book']
            customer = form.cleaned_data['customer']
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            entity = Borrow(book = book, customer = customer, start_date = start_date, end_date = end_date)
            entity.save()

            return redirect('borrows')

def removeborrow(request, entity_id):
    entity = get_object_or_404(Borrow, pk=entity_id)

    entity.delete()

    return redirect('borrows')

def genres(request):
    genres = Genre.objects.all()
    
    return render(request, 'base/genres.html', {'genres': genres})

def genre(request, genre_id):
    genre = get_object_or_404(Genre, pk=genre_id)
    books = Book.objects.filter(genre = genre)
    
    return render(request, 'base/genre.html', {'genre': genre, 'books': books})

def genreform(request):
    genre_form = GenreForm()
    
    return render(request, 'base/genreform.html', {'genre_form': genre_form})

def addgenre(request):
    if request.method == "POST":
        genre_form = GenreForm(request.POST)
        if genre_form.is_valid():
            name = genre_form.cleaned_data['name']
            genre = Genre(name = name)
            genre.save()

            return redirect('genres')

def removegenre(request, entity_id):
    entity = get_object_or_404(Genre, pk=entity_id)

    entity.delete()

    return redirect('genres')
    
# def addbook(request):
    